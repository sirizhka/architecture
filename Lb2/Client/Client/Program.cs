﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Client {
    internal class Program {
        private static string clientName;
        private static Socket clientConnection;
        private static int port = 7777;
        private static IPHostEntry host = Dns.GetHostEntry("127.0.0.1");
        
        public static void Main(string[] args) {
            Console.Write("Enter your nickname: ");
            clientName = Console.ReadLine();
            byte[] data = new byte[1024];

            try {
                IPEndPoint ipEndPoint = new IPEndPoint(host.AddressList[0], port);
                clientConnection = new Socket(host.AddressList[0].AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                
                clientConnection.Connect(ipEndPoint);
                
                clientConnection.Receive(data);
                Console.WriteLine(Encoding.UTF8.GetString(data));
                Console.WriteLine(clientName);
                clientConnection.Send(Encoding.UTF8.GetBytes(clientName));

                while (true) {
                    string message = Console.ReadLine();
                    clientConnection.Send(Encoding.UTF8.GetBytes(message));
                }
            }
            catch (Exception e) {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}