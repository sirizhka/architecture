﻿using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Client {    
    internal class Program {
        static public ArrayList _arrayList = new ArrayList();
        static public ArrayList _params = new ArrayList();
        static public void GetMethods(string url, string param = "") {
            var httpRequest = (HttpWebRequest) WebRequest.Create(url + param);
            httpRequest.Method = "GET";

            var httpResponse = httpRequest.GetResponse();
            var responseStream = httpResponse.GetResponseStream();
            var reader = new StreamReader(responseStream);
            var response = reader.ReadToEnd();

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(response);
            var nodeList = xmlDocument.GetElementsByTagName("wsdl:operation");

            foreach (XmlNode value in nodeList) {
                if (_arrayList.Contains(value.Attributes.GetNamedItem("name").Value)) {
                }
                else {
                    _arrayList.Add(value.Attributes.GetNamedItem("name").Value);
                }
            }
            
            nodeList = xmlDocument.GetElementsByTagName("soap12:body");
            Console.WriteLine("Params for methods: ");
            foreach (XmlNode value in nodeList) {
                if (!_params.Contains(value.Attributes.GetNamedItem("use").Value)) {
                    Console.WriteLine(value.Attributes.GetNamedItem("use").Value);
                    _params.Add(value.Attributes.GetNamedItem("use").Value);
                }
            }
            
            foreach (var VARIABLE in _arrayList) {
                Console.WriteLine("Method: " + VARIABLE);
            }
        }

        public static void Method(string url, string method, string param) {
            var httpRequest = (HttpWebRequest) WebRequest.Create(url + method + "?text=" + param);
            httpRequest.Method = "GET";

            var httpResponse = httpRequest.GetResponse();
            var responseStream = httpResponse.GetResponseStream();
            var reader = new StreamReader(responseStream);
            var response = reader.ReadToEnd();
            
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(response);
            var nodeList = xmlDocument.GetElementsByTagName("s");
            
            Console.WriteLine("May be you wanna write next words: ");
            foreach (XmlNode value in nodeList) {
                Console.WriteLine(value.InnerText);
            }
        }

        public static void Main(string[] args) {
            GetMethods("https://speller.yandex.net/services/spellservice?WSDL");
            Console.Write("Write method name: ");
            var method = Console.ReadLine();
            Console.Write("Write word to check: ");
            var word = Console.ReadLine();
            Method("https://speller.yandex.net/services/spellservice/", method, word);
        }
    }
}